# Maintainer: Alexey Min <alexey.min@gmail.com>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="postmarketos-qcom-sdm660"
pkgname=linux-$_flavor
pkgver=5.10_rc6
pkgrel=1
pkgdesc="Close to mainline linux kernel for Qualcomm Snapdragon 660 SoC"
arch="aarch64"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux-postmarketos"
_commit="5e9195254ef8c6c6f4870aa77057577f0c17d007"

source="
	$_repo-$_commit.tar.bz2::https://gitlab.com/postmarketOS/$_repo/-/archive/$_commit/$_repo-$_commit.tar.bz2
	config-$_flavor.aarch64
"

_carch="arm64"

builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp -v "$srcdir/config-$_flavor.$CARCH" "$builddir"/.config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	# bootloader requires compressed kernel
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz-$_flavor"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="
4a55855e60de655a77c43cbd4a08b611a7142235dbf5501007d734426e3d08291b8101b2e3e6e22306af8da52709fa4466c1ede6a0cd7b9d8d02eb250a09f5af  linux-postmarketos-5e9195254ef8c6c6f4870aa77057577f0c17d007.tar.bz2
06746eacbf7fb1b4d4e268d05dd76ddb18646018176b1ec6bc58d089768934613bbdaefb6e4c67dbe0b616c52ad1a28a733b3b744902f08dcf4ec1eb69370981  config-postmarketos-qcom-sdm660.aarch64
"
