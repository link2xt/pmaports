From 261684e74d4d00df63d7106162e2d0bb42e9652c Mon Sep 17 00:00:00 2001
From: Aleksander Morgado <aleksander@aleksander.es>
Date: Sun, 23 May 2021 12:19:06 +0200
Subject: [PATCH 05/39] base-modem: fix modem_sync() operation handling

The mm_base_modem_sync() method is an asynchronous method that
receives a callback and user data, and therefore we MUST always
complete the async method calling that callback. Set that up with a
GTask as usual.

Also, the mm_base_modem_sync_finish() method should be implemented
along with mm_base_modem_sync(), not in the source file of the
caller of the async method. The finish() always depends on how the
async method was implemented, in our case using a GTask.
---
 src/mm-base-manager.c |  7 -------
 src/mm-base-modem.c   | 34 +++++++++++++++++++++++++---------
 2 files changed, 25 insertions(+), 16 deletions(-)

diff --git a/src/mm-base-manager.c b/src/mm-base-manager.c
index 5af959fb..236405bb 100644
--- a/src/mm-base-manager.c
+++ b/src/mm-base-manager.c
@@ -669,13 +669,6 @@ mm_base_manager_num_modems (MMBaseManager *self)
 
 #if defined WITH_SYSTEMD_SUSPEND_RESUME
 
-gboolean mm_base_modem_sync_finish (MMBaseModem   *self,
-                                    GAsyncResult  *res,
-                                    GError       **error)
-{
-    return g_task_propagate_boolean (G_TASK (res), error);
-}
-
 static void
 base_modem_sync_ready (MMBaseModem  *self,
                        GAsyncResult *res,
diff --git a/src/mm-base-modem.c b/src/mm-base-modem.c
index 38f43f8c..923f159a 100644
--- a/src/mm-base-modem.c
+++ b/src/mm-base-modem.c
@@ -499,16 +499,26 @@ mm_base_modem_wait_link_port (MMBaseModem         *self,
 
 /******************************************************************************/
 
+gboolean
+mm_base_modem_sync_finish (MMBaseModem   *self,
+                           GAsyncResult  *res,
+                           GError       **error)
+{
+    return g_task_propagate_boolean (G_TASK (res), error);
+}
+
 static void
-mm_base_modem_sync_ready (MMBaseModem  *self,
-            GAsyncResult *res)
+sync_ready (MMBaseModem  *self,
+            GAsyncResult *res,
+            GTask        *task)
 {
-    g_autoptr (GError) error = NULL;
+    g_autoptr(GError) error = NULL;
 
-    MM_BASE_MODEM_GET_CLASS (self)->sync_finish (self, res, &error);
-    if (error) {
-        mm_obj_warn (self, "synchronization failed");
-    }
+    if (!MM_BASE_MODEM_GET_CLASS (self)->sync_finish (self, res, &error))
+        g_task_return_error (task, error);
+    else
+        g_task_return_boolean (task, TRUE);
+    g_object_unref (task);
 }
 
 void
@@ -516,14 +526,20 @@ mm_base_modem_sync (MMBaseModem         *self,
                     GAsyncReadyCallback  callback,
                     gpointer             user_data)
 {
+    GTask *task;
+
+    task = g_task_new (self, NULL, callback, user_data);
+
     g_assert (MM_BASE_MODEM_GET_CLASS (self)->sync != NULL);
     g_assert (MM_BASE_MODEM_GET_CLASS (self)->sync_finish != NULL);
 
     MM_BASE_MODEM_GET_CLASS (self)->sync (self,
-                                          (GAsyncReadyCallback) mm_base_modem_sync_ready,
-                                          NULL);
+                                          (GAsyncReadyCallback) sync_ready,
+                                          task);
 }
 
+/******************************************************************************/
+
 gboolean
 mm_base_modem_disable_finish (MMBaseModem   *self,
                               GAsyncResult  *res,
-- 
2.31.1

